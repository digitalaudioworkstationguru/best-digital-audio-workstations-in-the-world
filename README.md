# README #

Hey guys, this README is for all the music producers and software developers out there trying to either learn how to use a DAW or develop one.

More information @ https://bluebuzzmusic.com/the-best-daw-software-for-the-beginners/

### What is this repository for? ###

If you were ever to build a music studio at your home or have thought about it - it makes sense to do a proper research. Blue Buzz Music is here for this very reason - we are here to teach you everything you need to know about music production, gear, soft, and more! 

If you are looking to build a music studio on a budget - we got your back - it's easy as that - top notch articles about computers for music production, digital audio workstations, studio monitors, headphones, condenser and dynamic microphones and a lot more.

If you happen to be looking into DJing and building a DJ home setup, we got you as well - learn everything you need to know about controllers, mixers, DJ software, turntables, etc.

DAWs are software applications that run on your desktop or laptop computer and help you compose tracks in a creative manner - using VST plugins and samples for the beat, various generators for the loops and melodies, and recording the vocals or instruments using an audio interface and a condenser or dynamic microphone - easy as that. They include Ableton, FL Studio, Cubase, Reaper, Pro Tools, Logic Pro X, and many more digital software appplications for mucisians. 

### How do I get set up? ###

The set up is simpler than ever - simply go to the website above and figure everything out yourself - you are a music producer or a DJ afterall - adapting to the scary music industry is one of the things everyone has to do.


### Who do I talk to? ###

You can hit us up on the contact page at Blue Buzz Music - simple as that. While we get a lot of questions and inquiries, the most popular and commonly asked questions are usually outlined in the articles (the one about DAWs as an example). Anyways, if you happen to have more quesions about digital audio workstations - make sure to hit us up - we'll get back to you as soon as possible.